package com.massivecraft.factions.database;

import com.massivecraft.factions.Conf;

import lombok.Getter;

public class DatabaseManager {
	
	@Getter private static DatabaseManager instance; 
	
	public DatabaseManager() {
		instance = this;
	}
	
	public void connect() {
		new Database().connect(Conf.db_HOST, Conf.db_PORT, Conf.db_DATABASE, Conf.db_USERNAME, Conf.db_PASSWORD);
	}
	
	public void load() {
		Database.i.createTable("factions", 
				"id SMALLINT", 
				"name VARCHAR(32)",
				"leader VARCHAR(32)",
				"clan VARCHAR(32)",
				"members SMALLINT",
				"kills SMALLINT", 
				"deaths SMALLINT", 
				"claims SMALLINT", 
				"powers SMALLINT",
				"power_boost SMALLINT",
				"open SMALLINT",
				"ap SMALLINT", 
				"money INT", 
				"bonus INT", 
				"points INT");
	}
	
	public void save() {
		
	}
	
	public void disconnect() {
		Database.i.close();
	}

}
