package com.massivecraft.factions.adapters;

import java.lang.reflect.Type;

import com.massivecraft.factions.clan.Clan;

import net.minecraft.util.com.google.gson.JsonDeserializationContext;
import net.minecraft.util.com.google.gson.JsonDeserializer;
import net.minecraft.util.com.google.gson.JsonElement;
import net.minecraft.util.com.google.gson.JsonParseException;

public class ClanTypeAdapter implements JsonDeserializer<Clan> {

	@Override
	public Clan deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
		return Clan.parse(json.getAsString());
	}

}
