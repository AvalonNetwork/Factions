package com.massivecraft.factions.cmd;

import com.massivecraft.factions.*;
import com.massivecraft.factions.event.FPlayerJoinEvent;
import com.massivecraft.factions.event.FactionCreateEvent;
import com.massivecraft.factions.struct.Permission;
import com.massivecraft.factions.struct.Rel;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;

public class CmdCreate extends FCommand {
    public CmdCreate() {
        super();
        this.aliases.add("create");

        this.requiredArgs.add("faction tag");
        //this.optionalArgs.put("", "");

        this.permission = Permission.CREATE.node;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        String tag = this.argAsString(0);

        if (fme.hasFaction()) {
            msg("<b>Vous devez d'abord quitter votre faction!");
            return;
        }

        if (Factions.i.isTagTaken(tag)) {
            msg("<b>Une faction de se nom existe d�j�!");
            return;
        }

        ArrayList<String> tagValidationErrors = Factions.validateTag(tag);
        if (tagValidationErrors.size() > 0) {
            sendMessage(tagValidationErrors);
            return;
        }
        
        // trigger the faction creation event (cancellable)
        FactionCreateEvent createEvent = new FactionCreateEvent(me, tag);
        Bukkit.getServer().getPluginManager().callEvent(createEvent);
        if (createEvent.isCancelled()) return;

        Faction faction = Factions.i.create();

        // TODO: Why would this even happen??? Auto increment clash??
        if (faction == null) {
            msg("<b>There was an internal error while trying to create your faction. Please try again.");
            return;
        }

        // finish setting up the Faction
        faction.setTag(tag);

        // trigger the faction join event for the creator
        FPlayerJoinEvent joinEvent = new FPlayerJoinEvent(FPlayers.i.get(me), faction, FPlayerJoinEvent.PlayerJoinReason.CREATE);
        Bukkit.getServer().getPluginManager().callEvent(joinEvent);
        // join event cannot be cancelled or you'll have an empty faction

        // finish setting up the FPlayer
        fme.setRole(Rel.LEADER);
        fme.setFaction(faction);

        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "�9" + fme.getName() + " &7vient de cr�er la faction &c" + faction.getTag() + "&7."));
        
        message("&7Pour choisir votre clan faites &9/clan <clan>");

        if (Conf.logFactionCreate)
            P.p.log(fme.getName() + " created a new faction: " + tag);
    }

}
