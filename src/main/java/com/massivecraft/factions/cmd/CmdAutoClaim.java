package com.massivecraft.factions.cmd;

import com.massivecraft.factions.Faction;
import com.massivecraft.factions.struct.FPerm;
import com.massivecraft.factions.struct.Permission;

public class CmdAutoClaim extends FCommand {
    public CmdAutoClaim() {
        super();
        this.aliases.add("autoclaim");

        this.optionalArgs.put("faction", "vous");

        this.permission = Permission.AUTOCLAIM.node;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        Faction forFaction = this.argAsFaction(0, myFaction);
        if (forFaction == null || forFaction == fme.getAutoClaimFor()) {
            fme.setAutoClaimFor(null);
            msg("<i>Vous venez de désactiver l'auto-claim!");
            return;
        }

        if (!FPerm.TERRITORY.has(fme, forFaction, true)) return;

        fme.setAutoClaimFor(forFaction);

        msg("<i>Vous venez d'activer l'auto-claim, le prochain territoire sera claim pour la faction <h>%s<i>.", forFaction.describeTo(fme));
        fme.attemptClaim(forFaction, me.getLocation(), true);
    }

}