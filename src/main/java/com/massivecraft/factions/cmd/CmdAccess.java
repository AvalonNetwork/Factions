package com.massivecraft.factions.cmd;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.struct.FPerm;
import com.massivecraft.factions.struct.Permission;
import com.massivecraft.factions.struct.TerritoryAccess;
import com.massivecraft.factions.zcore.util.TextUtil;

public class CmdAccess extends FCommand {
    public CmdAccess() {
        super();
        this.aliases.add("access");
        this.aliases.add("owner");

        this.optionalArgs.put("view|p|f|player|faction", "view");
        this.optionalArgs.put("name", "you");

        this.setHelpShort("Restreint les permissions de build/d�truire dans un territoire.");

        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeOfficer = false;
        senderMustBeLeader = false;
    }

    @Override
    public void perform() {
        String type = this.argAsString(0);
        type = (type == null) ? "" : type.toLowerCase();
        FLocation loc = new FLocation(me.getLocation());

        TerritoryAccess territory = Board.getTerritoryAccessAt(loc);
        Faction locFaction = territory.getHostFaction();
        boolean accessAny = Permission.ACCESS_ANY.has(sender, false);

        if (type.isEmpty() || type.equals("view")) {
            if (!accessAny && !Permission.ACCESS_VIEW.has(sender, true)) return;
            if (!accessAny && !territory.doesHostFactionMatch(fme)) {
                msg("<b>Ce territoire ne vous appartient pas!");
                return;
            }
            showAccessList(territory, locFaction);
            return;
        }

        if (!accessAny && !Permission.ACCESS.has(sender, true)) return;
        if (!accessAny && !FPerm.ACCESS.has(fme, locFaction, true)) return;

        boolean doPlayer = true;
        if (type.equals("f") || type.equals("faction")) {
            doPlayer = false;
        } else if (!type.equals("p") && !type.equals("player")) {
            msg("<b>Vous devez sp�cifier \"p\" ou \"player\" pour access un joueur et \"f\" ou \"faction\" pour access une faction.");
            msg("<b>Exemple: /f access p IceExpert ou /f access f Avalon");
            return;
        }

        boolean added;

        if (doPlayer) {
            FPlayer targetPlayer = this.argAsBestFPlayerMatch(1, fme);
            if (targetPlayer == null) return;
            added = territory.toggleFPlayer(targetPlayer);
            message("&9" + targetPlayer.getName() + " " + (added ? "&aajouter" : "&csupprimer") + " &7des owner."); 
        } else {
            Faction targetFaction = this.argAsFaction(1, myFaction);
            if (targetFaction == null) return;
            added = territory.toggleFaction(targetFaction);
            message("&9" + targetFaction.getTag() + " " + (added ? "&aajouter" : "&csupprimer") + " &7des factions owner!"); 
        }
    }

    private void showAccessList(TerritoryAccess territory, Faction locFaction) {
    	if(territory.size() <= 0) {
    		message("&cIl n'y a aucun syst�me d'owner sur ce territoire!");
    		return;
    	}
    	
    	message(" ");
    	message(TextUtil.LINE);
    	message(" ");
    	
    	message("&8> &3" + territory.size() + " &7joueur(s) ont acc�s � ce territoire&8.");
    	
    	message(" ");
    	
        String players = territory.fplayerList();
        if(players.isEmpty()) 
        	players = "&cAucun joueur n'a acc�s � ce territoire.";
    	message("&8� &7Joueur(s): &3" + players);
    	
    	message(" ");
    	
        String factions = territory.factionList();
        if(factions.isEmpty())
        	factions = "&cAucune faction n'a acc�s � ce territoire.";
        message("&8� &7Faction(s): &d" + factions);
        
    	message(" ");
    	message(TextUtil.LINE);
    	message(" ");
    }
}
