package com.massivecraft.factions.clan;

public enum Clan {
	
	VALLOST,
	
	GHAEILL,
	
	;
	
    public static Clan parse(String str) {
        if (str == null || str.length() < 1) return null;
        str = str.toLowerCase();
        
        if(str.equals("vallost")) return VALLOST;
        
        if(str.equals("ghaeill")) return GHAEILL;
        
        return null;
    }
}
